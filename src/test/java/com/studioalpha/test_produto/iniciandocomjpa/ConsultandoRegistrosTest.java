package com.studioalpha.test_produto.iniciandocomjpa;

import org.junit.Assert;
import org.junit.Test;

import com.studioalpha.model.Produto;
import com.studioalpha.test_produto.EntityManagerTest;

	public class ConsultandoRegistrosTest extends EntityManagerTest {

	    @Test
	    public void busarPorIdentificador() {
	        Produto produto = entityManager.find(Produto.class, 1);
//	        Produto produto = entityManager.getReference(Produto.class, 1);

	        Assert.assertNotNull(produto);
	        Assert.assertEquals("Kindle", produto.getNome());
	    }

	    @Test
	    public void atualizarAReferencia() {
	        Produto produto = entityManager.find(Produto.class, 1);
	        produto.setNome("Microfone Samson");

	        entityManager.refresh(produto);

	        Assert.assertEquals("Kindle", produto.getNome());
	    }
	}