package com.studioalpha.service;

import java.io.Serializable;

import com.studioalpha.model.Cliente;
import com.studioalpha.repository.Clientes;
import com.studioalpha.util.jpa.Transactional;

import jakarta.inject.Inject;

public class CadastroClienteService implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5892428952338322158L;
	@Inject
	private Clientes clientes;
	
	@Transactional
	public Cliente salvar(Cliente cliente) throws NegocioException {
		return clientes.guardar(cliente);
	}

}
