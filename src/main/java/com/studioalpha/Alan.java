package com.studioalpha;


import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;

import jakarta.inject.Named;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Named
@SessionScoped
public class Alan implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 735488649373947689L;
	private List<String> list;
    private String name;

    @PostConstruct
    public void init(){
        list = new ArrayList<>();
    }

    public void add(){
        list.add(name);
    }

    public void remove(String e){
        list.removeIf( x -> x.equals(e));
    }

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(list, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alan other = (Alan) obj;
		return Objects.equals(list, other.list) && Objects.equals(name, other.name);
	}
    
    
}
