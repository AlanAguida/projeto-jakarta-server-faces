package com.studioalpha.persistencia.util;
import com.studioalpha.model.Produto;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class IniciarUnidadeDePersistencia {
	
	public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence
                .createEntityManagerFactory("PautaSysDS");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        Produto produto = entityManager.find(Produto.class, 1);
        try {
        System.out.println(produto.getNome());
        }catch(Exception e) {
        	System.out.println(e.getMessage());
        	
        }
        entityManager.close();
        entityManagerFactory.close();
    }

}
