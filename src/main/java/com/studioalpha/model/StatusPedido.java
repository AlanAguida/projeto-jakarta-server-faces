package com.studioalpha.model;

public enum StatusPedido {

	ORCAMENTO, EMITIDO, CANCELADO
	
}
